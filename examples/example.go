package main

import (
	"log"

	"github.com/mraraneda/scriblog"
)

func main() {
	useZap()
	useLogrus()
}

func useLogrus() {
	fileConfig := scriblog.LogrusFileConfiguration{
		Enable:     true,
		Level:      scriblog.Debug,
		JSONFormat: false,
		Path:       "log.log",
	}

	consoleConfig := scriblog.LogrusConsoleConfiguration{
		Enable:     true,
		Level:      scriblog.Debug,
		JSONFormat: false,
	}

	config := scriblog.Configuration{
		scriblog.LogrusConsoleConfig: consoleConfig,
		scriblog.LogrusFileConfig:    fileConfig,
	}

	err := scriblog.NewLogger(config, scriblog.InstanceLogrusLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
	}
	contextLogger := scriblog.WithFields(scriblog.Fields{"animal": "walrus"})
	contextLogger.Debugf("Starting with logrus")
	contextLogger.Infof("Logrus is awesome")
}

func useZap() {
	fileConfig := scriblog.ZapFileConfiguration{
		Enable:     true,
		Level:      scriblog.Debug,
		JSONFormat: false,
		Path:       "log.log",
	}

	consoleConfig := scriblog.ZapConsoleConfiguration{
		Enable:     true,
		Level:      scriblog.Debug,
		JSONFormat: false,
	}

	config := scriblog.Configuration{
		scriblog.ZapConsoleConfig: consoleConfig,
		scriblog.ZapFileConfig:    fileConfig,
	}

	err := scriblog.NewLogger(config, scriblog.InstanceZapLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
	}
	contextLogger := scriblog.WithFields(scriblog.Fields{"zap": "thunder"})
	contextLogger.Debugf("Starting with zap")
	contextLogger.Infof("Zap is awesome")
}
